import App from "next/app"
import { ThemeProvider } from "styled-components"

export function reportWebVitals(metric) {
  console.log(metric)
}
const theme = {
  colors: {
    primary: "rgb(38, 38, 38)",
    secondary: "rgb(242, 242, 242)",
    tetriary: "rgb(227, 227, 227)",
    contrast: "rgb(255, 255, 255)",
    hyperlink: "rgb(87, 187, 191)",

    contactText: "rgb(85, 85, 85)",
    contactTextError: "rgb(192, 36, 37)",
  },
}

export default class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props
    return (
      <ThemeProvider theme={theme}>
        <Component {...pageProps} />
      </ThemeProvider>
    )
  }
}
