import Head from "next/head"
import { Container } from "reactstrap"
import { createGlobalStyle } from "styled-components"

const GlobalHomeStyle = createGlobalStyle`

  @import url("https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;500;700&display=swap");
body {
    margin: 0px;
    font-family: "Noto Sans JP", sans-serif;
    color: $primary-color;
  
    a {
      color: $hyperlink-color;
      text-decoration: none;
      &:visited {
        text-decoration: none;
        color: $hyperlink-color;
      }
    }
  
    .container {
      .app-links {
        display: flex;
        flex-direction: row;
        justify-content: space-around;
        max-width: 302px;
  
        #ios-link-banner {
          margin-right: 10px;
        }
        #ios-link-footer {
          margin-right: 10px;
        }
  
        @media only screen and (max-width: 800px) {
          flex-direction: column;
  
          #ios-link-banner {
            margin-right: 0px;
          }
          #ios-link-footer {
            margin-right: 0px;
          }
        }
      }
  
      .title {
        font-weight: bold;
        font-size: 20px;
      }
      .logo-header {
        padding-top: 40px;
        padding-left: 135px;
  
        @media only screen and (max-width: 800px) {
          padding-top: 16px;
          padding-left: 16px;
          img {
            width: 40px;
            height: 40px;
            object-fit: cover;
          }
        }
      }
  
      .banner {
        max-width: 1440px;
        margin: auto;
        height: 40vw;
        max-height: 560px;
        min-height: 250px;
        background-position: right;
        background-size: contain;
        background-repeat: no-repeat;
        display: flex;
        justify-content: flex-end;
        align-items: center;
        .no-webp & {
          background-image: url("/banner-1.png");
        }
        .webp & {
          background-image: url("/banner-1.webp");
        }
        .banner-title {
          @extend .title;
          font-size: 40px;
          text-align: center;
          display: flex;
          flex-direction: column;
          align-items: center;
          width: 320px;
          padding-right: 63%;
          div:last-child {
            margin-top: 24px;
          }
        }
  
        //tablet
        @media only screen and (max-width: 1024px) {
          .banner-title {
            font-size: 24px;
            width: 240px;
          }
        }
  
        //mobile
        @media only screen and (max-width: 600px) {
          min-height: 400px;
          max-height: 560px;
          height: 100vw;
          align-items: flex-start;
          .no-webp & {
            background-image: url("/banner-mobile-1.png");
          }
          .webp & {
            background-image: url("/banner-mobile-1.webp");
          }
          .banner-title {
            font-size: 24px;
            padding-right: 48%;
            padding-top: 49%;
            width: 192px;
            div:last-child {
              margin-top: 16px;
            }
          }
        }
  
        //extra-small mobile
        @media only screen and (max-width: 374px) {
          background-size: cover;
          .banner-title {
            font-size: 22px;
            padding-right: 45%;
            padding-top: 65%;
          }
        }
      }
  
      .description {
        margin: 40px 16px 80px 16px;
        font-size: 14px;
        text-align: center;
        line-height: 30px;
        div:first-child {
          font-size: 20px;
          margin-bottom: 24px;
          font-weight: bold;
        }
      }
  
      .invitation {
        margin: auto;
        padding-top: 40px;
        padding-bottom: 64px;
        max-width: 1440px;
        display: grid;
        background: linear-gradient(
          to right,
          $secondary-color,
          $secondary-color 41%,
          $contrast-color 41%
        );
        background: -moz-linear-gradient(
          to right,
          $secondary-color,
          $secondary-color 41%,
          $contrast-color 41%
        );
        grid-template-columns: minmax(250px, 887px) minmax(275px, 390px);
        grid-template-rows: 40px 437px;
        grid-gap: 40px 48px;
        grid-auto-flow: column;
  
        .invitation-aligner {
          position: absolute;
          left: calc(50% - 20px);
          display: flex;
          flex-direction: column;
          align-items: center;
          img {
            position: relative;
            bottom: 80px;
          }
        }
        .invitation-title {
          @extend .title;
          display: flex;
          align-items: center;
          padding-right: 20px;
        }
  
        .invitation-banner {
          width: 100%;
          height: 100%;
          object-fit: cover;
        }
  
        .invitation-text {
          font-size: 14px;
          line-height: 30px;
          padding-right: 20px;
  
          p {
            margin: 0px;
          }
        }
  
        //tablet
        @media only screen and (max-width: 1024px) {
          grid-gap: 20px 24px;
          .invitation-title {
            padding-left: 32px;
            font-size: 16px;
          }
          .invitation-text {
            font-size: 13px;
          }
        }
  
        //mobile
        @media only screen and (max-width: 600px) {
          display: flex;
          background: $contrast-color;
          padding: 24px 16px 40px 16px;
          flex-direction: column;
          br {
            display: none;
          }
  
          .invitation-aligner {
            img {
              position: relative;
              bottom: 48px;
              &:first-child {
                height: 48px;
              }
            }
          }
  
          .invitation-banner {
            margin-top: 64px;
          }
  
          .invitation-title {
            padding: unset;
            justify-content: center;
            margin: 24px 0px;
          }
  
          .invitation-text {
            padding-right: 0px;
          }
        }
      }
  
      .instruction {
        max-width: 1440px;
        margin: auto;
        padding-bottom: 80px;
  
        @media only screen and (max-width: 600px) {
          padding-bottom: 64px;
        }
  
        .instruction-title {
          @extend .title;
          text-align: center;
          margin: 40px 0px;
        }
  
        .instruction-step {
          display: grid;
          grid-template-columns: minmax(288px, 708px) minmax(288px, 708px);
          grid-column-gap: 24px;
  
          .phone-side {
            display: flex;
            justify-content: flex-end;
            padding-top: 100px;
            min-height: 481px;
            background-image: url("/Black-Left.svg");
            background-repeat: no-repeat;
            background-position: top right 120px;
            grid-column: 1;
            grid-row: 1;
            img {
              border-radius: 40px / 40px;
              box-shadow: 0px 2px 18px rgba(0, 0, 0, 0.12);
            }
          }
  
          .content-side {
            padding-top: 207px;
            grid-column: 2;
  
            .step-content {
              margin-left: 50px;
  
              .step-logo {
                width: 80px;
                height: 80px;
                object-fit: cover;
                margin-bottom: 24px;
              }
  
              .step-title {
                @extend .title;
                font-size: 16px;
                margin-bottom: 16px;
              }
  
              .step-text {
                font-size: 14px;
                margin-bottom: 16px;
                max-width: 460px;
                padding-right: 24px;
              }
            }
  
            .step-arrow {
              width: 100px;
              height: 34px;
              object-fit: cover;
            }
          }
  
          @media only screen and (max-width: 600px) {
            display: flex;
            flex-direction: column;
            margin-bottom: 64px;
            .phone-side {
              display: flex;
              justify-content: center;
              padding-top: 24px;
              background-position: top right 50vw;
            }
  
            .content-side {
              padding-top: 0px;
              display: flex;
              flex-direction: column;
              align-items: center;
  
              .step-content {
                text-align: center;
                margin-left: 0px;
  
                .step-logo {
                  margin-bottom: 16px;
                }
  
                .step-text {
                  max-width: 295px;
                  padding-right: 0px;
                }
              }
  
              .step-arrow {
                display: none;
              }
            }
          }
  
          &--reverse {
            display: grid;
          grid-template-columns: minmax(288px, 708px) minmax(288px, 708px);
          grid-column-gap: 24px;

            .phone-side {
              justify-content: flex-start;
              background-image: url("/Black-Right.svg");
              background-position: top left 120px;
              grid-column: 2;
            }
  
            .content-side {
              grid-column: 1;
              display: flex;
              flex-direction: column;
              align-items: flex-end;
              padding-left: 24px;
  
              .step-content {
                margin-right: 50px;
                margin-left: 0px;
  
                .step-text {
                  padding-right: 0px;
                }
              }
            }
  
            @media only screen and (max-width: 600px) {
              .phone-side {
                display: flex;
                justify-content: center;
                padding-top: 24px;
                background-position: top left 50vw;
              }
  
              .content-side {
                padding-top: 0px;
                padding-left: 0px;
                display: flex;
                flex-direction: column;
                align-items: center;
  
                .step-content {
                  text-align: center;
                  margin-right: 0px;
  
                  .step-logo {
                    margin-bottom: 16px;
                  }
  
                  .step-text {
                    max-width: 295px;
                    padding-right: 0px;
                  }
                }
  
                .step-arrow {
                  display: none;
                }
              }
            }
          }
        }
      }
  
      .footer-banner {
        background-color: $contrast-color;
        padding: 80px 128px;
        display: flex;
        flex-direction: column;
        align-items: center;
        .app-links {
          margin-top: 24px;
        }
      }
  
      .footer-menu {
        background-color: $primary-color;
        font-size: 12px;
        padding: 12px 0px;
        div {
          max-width: 744px;
          display: flex;
          margin: auto;
          justify-content: space-between;
          a {
            color: $tetriary-color;
          }
        }
  
        @media only screen and (max-width: 800px) {
          padding: 40px 20px;
          div {
            max-width: unset;
            display: flex;
            flex-direction: column;
            align-items: center;
            a {
              color: $tetriary-color;
              margin-bottom: 24px;
              &:last-child {
                margin-bottom: 0px;
              }
            }
          }
        }
      }
    }
  }
`

export default function Home(props) {
  return (
    <>
      <Head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>SHAiRE</title>
        <meta property="og:title" name="title" content="SHAiRE" />
        <meta
          property="og:description"
          name="description"
          content="SHAiREは、GO TODAY SAHiRE SALONをご利用頂くお客様の為のスマホアプリです。
        事前にお支払い情報を登録しておくことで、スムーズなキャッシュレス決済が行えます。
        アプリひとつでスマートなお会計ができます。もちろん現金決済にも対応。
        またアプリを使えば、今までの施術履歴や決済履歴などもスマートフォンで確認することができます。
        今後はポイントや予約などの機能を追加予定です。"
        />
        <meta property="og:image" name="image" content="/banner-1.png" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="SHAiRE" />
        <meta
          name="twitter:description"
          content="SHAiREは、GO TODAY SAHiRE SALONをご利用頂くお客様の為のスマホアプリです。
        事前にお支払い情報を登録しておくことで、スムーズなキャッシュレス決済が行えます。
        アプリひとつでスマートなお会計ができます。もちろん現金決済にも対応。
        またアプリを使えば、今までの施術履歴や決済履歴などもスマートフォンで確認することができます。
        今後はポイントや予約などの機能を追加予定です。"
        />
        <meta name="twitter:image" content="/banner-1.png" />
      </Head>
      <GlobalHomeStyle />
      <Container>
        <div>
          <div className="banner">
            <div className="banner-title">
              <div>担当美容師と</div>
              <div>「もっと繋がる」</div>
              <div>スマートアプリ</div>
              <div className="app-links">
                <a
                  id="ios-link-banner"
                  href="https://www.apple.com/ios/app-store/"
                >
                  <img src="ios.svg" alt="ios" />
                </a>
                <a
                  id="android-link-banner"
                  href="https://play.google.com/store?hl=en"
                >
                  <img src="android.svg" alt="android" />
                </a>
              </div>
            </div>
          </div>
          <div className="description">
            <div> SHAiRE(シェア) とは？ </div>
            <div>
              {" "}
              SHAiREは、GO TODAY SAHiRE
              SALONをご利用頂くお客様の為のスマホアプリです。{" "}
            </div>
            <div>
              {" "}
              事前にお支払い情報を登録しておくことで、スムーズなキャッシュレス決済が行えます。{" "}
            </div>
            <div>
              {" "}
              アプリひとつでスマートなお会計ができます。もちろん現金決済にも対応。{" "}
            </div>
            <div>
              {" "}
              またアプリを使えば、今までの施術履歴や決済履歴などもスマートフォンで確認することができます。{" "}
            </div>
            <div> 今後はポイントや予約などの機能を追加予定です。 </div>
          </div>
          <div className="invitation">
            <div className="invitation-aligner">
              <img src="line.svg" alt="Center" />
              <img src="Logo-2.svg" alt="GO TO SHAiRE" />
            </div>
            <br />
            <picture>
              <source type="image/webp" srcSet="banner-2.webp" />
              <source type="image/png" srcSet="banner-2.png" />
              <img
                className="invitation-banner"
                src="banner-2.png"
                alt="Shaire"
              />
            </picture>
            <div className="invitation-title">
              GO TODAY SHAiRE SALONについて
            </div>
            <div className="invitation-text">
              <p>
                GO TODAY SHAiRE SALONは美容室のあり方を変えることで、
                もっと自由に、もっと楽しく、もっと心地よくお客様が過ごせるように、
                美容室のエンターテイメントを創造していきます。
                お客様と「喜び」「感動」「ワクワク感」をシェアし、
                お客様に多くの笑顔と満足を提供し、
                固定概念にとらわれない美容師とお客様が主役の美容室を作り続けていきます。
                ゆったりとした個室空間（一部オープン席あり）と充実した設備の店舗で、
                自分だけの空間、自分だけの時間が過ごせるシェアサロンです。
              </p>
              <br />
              <p>
                GO TODAY SHAiRE
                SALONは、全国に15店舗展開しているヘアサロンです。
                サロンの詳細やご予約に関しては
                <a href="https://www.gotoday-shaire.salon/">公式ホームページ</a>
                をご確認ください。
              </p>
            </div>
          </div>
          <div className="instruction">
            <div className="instruction-title"> ご利用方法 </div>
            <div className="instruction-step">
              <div className="content-side">
                <div className="step-content">
                  <img className="step-logo" src="Icon-1.svg" alt="QR" />
                  <div className="step-title"> QRコードを表示 </div>
                  <div className="step-text">
                    {" "}
                    施術が完了したら、アプリに表示されているQRコードを表示して担当美容師に画面を見せましょう。{" "}
                  </div>
                </div>
                <img className="step-arrow" src="Left-Arrow.svg" alt="left" />
              </div>
              <div className="phone-side">
                <img src="step-1.svg" alt="QR" />
              </div>
            </div>
            <div className="instruction-step--reverse">
              <div className="content-side">
                <div className="step-content">
                  <img className="step-logo" src="Icon-2.svg" alt="Card" />
                  <div className="step-title"> 決済方法の選択 </div>
                  <div className="step-text">
                    {" "}
                    本日の施術内容を確認し、決済方法をご選択ください。{" "}
                  </div>
                </div>
                <img className="step-arrow" src="Right-Arrow.svg" alt="right" />
              </div>
              <div className="phone-side">
                <img src="step-2.svg" alt="Card" />
              </div>
            </div>
            <div className="instruction-step">
              <div className="content-side">
                <div className="step-content">
                  <img className="step-logo" src="Icon-3.svg" alt="Receipt" />
                  <div className="step-title"> お会計完了 </div>
                  <div className="step-text">
                    {" "}
                    決済の完了画面が表示されたら、お会計が完了します。お会計内容は、アプリの「履歴」画面からもご確認いただけます。{" "}
                  </div>
                </div>
                <img className="step-arrow" src="Left-Arrow.svg" alt="left" />
              </div>
              <div className="phone-side">
                <img src="step-3.svg" alt="Receipt" />
              </div>
            </div>
          </div>
        </div>
      </Container>
    </>
  )
}

// export async function getStaticProps({ preview = null }) {
// const somethingAsync = (await asyncFn(preview)) || []
// return {
// props: { allPosts, preview },
// }
// }
