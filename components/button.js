import React from "react"
import { Button } from "reactstrap"
import styled from "styled-components"

const Button = (props) => {
  return (
    <div>
      <Button color="primary">{props.name}</Button>
    </div>
  )
}

export default styled.Button``
