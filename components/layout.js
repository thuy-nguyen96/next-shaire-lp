import React from "react"
import styled from "styled-components"
import Head from "next/head"
import Link from "next/link"

function Header(props) {
  return (
    <Head>
      <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
      <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      {/* 
        Attempting to preload font in react with <link onload> is not availiable
        see more at: https://github.com/facebook/create-react-app/issues/3319
      */}
      <link
        href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;500;700&display=swap"
        rel="stylesheet"
        type="text/css"
      />
      <title>{props.title}</title>
      <meta property="og:title" name="title" content="SHAiRE" />
      <meta
        property="og:description"
        name="description"
        content="SHAiREは、GO TODAY SAHiRE SALONをご利用頂くお客様の為のスマホアプリです。
      事前にお支払い情報を登録しておくことで、スムーズなキャッシュレス決済が行えます。
      アプリひとつでスマートなお会計ができます。もちろん現金決済にも対応。
      またアプリを使えば、今までの施術履歴や決済履歴などもスマートフォンで確認することができます。
      今後はポイントや予約などの機能を追加予定です。"
      />
      <meta property="og:image" name="image" content="/banner-1.png" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:title" content="SHAiRE" />
      <meta
        name="twitter:description"
        content="SHAiREは、GO TODAY SAHiRE SALONをご利用頂くお客様の為のスマホアプリです。
      事前にお支払い情報を登録しておくことで、スムーズなキャッシュレス決済が行えます。
      アプリひとつでスマートなお会計ができます。もちろん現金決済にも対応。
      またアプリを使えば、今までの施術履歴や決済履歴などもスマートフォンで確認することができます。
      今後はポイントや予約などの機能を追加予定です。"
      />
      <meta name="twitter:image" content="/banner-1.png" />
    </Head>
  )
}

const LogoHeader = styled.div`
  padding-top: 40px;
  padding-left: 135px;

  @media only screen and (max-width: 800px) {
    padding-top: 16px;
    padding-left: 16px;
    img {
      width: 40px;
      height: 40px;
      object-fit: cover;
    }
  }
`
const FooterBanner = styled.div`
  background-color: ${({ theme }) => theme.colors.contrast};
  padding: 80px 128px;
  display: flex;
  flex-direction: column;
  align-items: center;
  .app-links {
    margin-top: 24px;
  }
`
const Container = styled.div`
  background-color: ${({ theme }) => theme.colors.secondary};
`

function Logo() {
  return (
    <LogoHeader>
      <a href="/">
        <img src="Logo-1.svg" alt="SHAiRE" />
      </a>
    </LogoHeader>
  )
}
function Footer() {
  return (
    <FooterBanner>
      <a href="/">
        <img src="Logo-1.svg" alt="SHAiRE" />
      </a>
      <div class="app-links">
        <a id="ios-link-footer" href="https://www.apple.com/ios/app-store/">
          <img src="ios.svg" alt="ios" />
        </a>
        <a id="android-link-footer" href="https://play.google.com/store?hl=en">
          <img src="android.svg" alt="android" />
        </a>
      </div>
    </FooterBanner>
  )
}

const FooterNav = styled.div`
  background-color: ${({ theme }) => theme.colors.primary};
  font-size: 12px;
  padding: 12px 0px;
  div {
    max-width: 744px;
    display: flex;
    margin: auto;
    justify-content: space-between;
    a {
      color: ${({ theme }) => theme.colors.tetriary};
    }
  }

  @media only screen and (max-width: 800px) {
    padding: 40px 20px;
    div {
      max-width: unset;
      display: flex;
      flex-direction: column;
      align-items: center;
      a {
        color: ${({ theme }) => theme.colors.tetriary};
        margin-bottom: 24px;
        &:last-child {
          margin-bottom: 0px;
        }
      }
    }
  }
`

export default function Layout({ title, children }) {
  const pages = [
    { href: "faq", name: " ガイド (Q&A)" },
    { href: "terms-customer.html", name: "利用規約" },
    { href: "privacy-policies", name: "プライバシーポリシー" },
    { href: "company-info", name: "会社概要" },
    { href: "contact", name: "お問い合わせ" },
  ]
  return (
    <>
      <Header title={title} />
      <Logo />
      <Container>
        <>{children}</>
      </Container>
      <Footer />
      <FooterNav>
        <div>
          {pages.map((pg) => (
            <Link href={`/${pg.href}`} key={`${pg.href}__${pg.name}`}>
              <a>{pg.name}</a>
            </Link>
          ))}
        </div>
      </FooterNav>
    </>
  )
}
